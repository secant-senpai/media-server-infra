ARG BASE_IMAGE=xonsh

FROM xonsh/${BASE_IMAGE}:slim

COPY --from=docker:27.3.1 /usr/local/bin/docker /usr/bin/docker
COPY --from=docker:27.3.1 /usr/local/bin/docker-compose /usr/bin/docker-compose
COPY --from=docker.io/infisical/cli:0.34.2 /bin/infisical /usr/local/bin/infisical

# Needed dependencies
RUN apt-get update \
    && apt-get install -y curl \
    && rm -rf /var/lib/apt/lists/*

# Go-Task
RUN sh -c "$(curl --location https://taskfile.dev/install.sh)" -- -d -b /usr/local/bin

RUN xpip install python-dotenv

# Needed for VSCode xonsh extension to work
RUN xpip install python-lsp-server[all]

WORKDIR /data
