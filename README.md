# Media Server Infra

Home Docker Infrastructure.

## Components

- [Forgejo](https://forgejo.org/) -> Self-hosted Git forge
- [Adguard Home](https://adguard.com/en/adguard-home/overview.html) -> DNS Server
- [Caddy](https://caddyserver.com/) -> Reverse Proxy
- [Homepage](https://gethomepage.dev/) -> Landing Page
- [Infisical](https://infisical.com/) -> Secrets Store
- [Jellyfin](https://jellyfin.org/), Jellyseerr, Starr -> Media Server Stack
- [Prometheus](https://prometheus.io/), [Grafana](https://grafana.com/) -> Monitoring Stack (Metrics)
- [Loki](https://grafana.com/oss/loki/), Promtail -> Monitoring Stack (Logging)
