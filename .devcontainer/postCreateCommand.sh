#!/usr/bin/env bash
set -e
set -o noglob

# Checks installed versions
python --version
xonsh --version
