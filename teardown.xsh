#!/usr/bin/env xonsh

# teardown.xsh - A script to remove all Docker Compose deployments.
# Version: 1.0
# Author: Secant_Senpai
# Description: A script to remove deployments specified in `.deployables` file.
# Usage: xonsh teardown.xsh
# Example: cat teardown.xsh

# Changelog:
# Version 1.0 - [2025-02-17]: Initial release.

# Version information
SCRIPT_VERSION="1.0"

$RAISE_SUBPROC_ERROR = True
$XONSH_TRACEBACK_LOGFILE = "xonsh-traceback.err"

assert !(which docker).returncode == 0, "Script requires `docker` but it is not installed. Aborting." or exit 1

if p".deployables".is_file():
    with open(".deployables", "r") as file:
        DOT_DEPLOYABLES = [line.strip() for line in file]
else:
    DOT_DEPLOYABLES = []

for app in DOT_DEPLOYABLES:
    # Teardown composition
    cd @(app)
    task down
    cd ..
