#!/usr/bin/env xonsh

# init.xsh - A script to start a Docker Compose stack with secrets from Infisical.
# Version: 1.0
# Author: Secant_Senpai
# Description: A initialization script to authenticate against Infisical API, then ingests secrets into deployments specified in the `.deployables` file.
# Usage: xonsh init.xsh {app1|app1 app2}
# Example: cat init.xsh adguardhome caddy

# Changelog:
# Version 1.0 - [2025-02-17]: Initial release.

# Version information
SCRIPT_VERSION="1.0"

$RAISE_SUBPROC_ERROR = True
$XONSH_TRACEBACK_LOGFILE = "xonsh-traceback.err"

assert !(which docker).returncode == 0, "Script requires `docker` but it is not installed. Aborting." or exit 1

import os
import tempfile
from dotenv import dotenv_values
from pathlib import Path

# Get auth credentials
dotenv_global = dotenv_values(".env")
INFISICAL_CLIENT_ID = dotenv_global.get("INFISICAL_CLIENT_ID")
INFISICAL_CLIENT_SECRET = dotenv_global.get("INFISICAL_CLIENT_SECRET")
INFISICAL_ENV = dotenv_global.get("INFISICAL_ENV")

$INFISICAL_API_URL = dotenv_global.get("INFISICAL_API_URL")
$INFISICAL_TOKEN=$(infisical login --method=universal-auth --client-id=@(INFISICAL_CLIENT_ID) --client-secret=@(INFISICAL_CLIENT_SECRET) --silent --plain)

def obtain_list_of_apps(applications: list) -> list:
    if p".deployables".is_file():
        with open(".deployables", "r") as file:
            DOT_DEPLOYABLES = [line.strip() for line in file]
    else:
        DOT_DEPLOYABLES = []

    match len(applications):
        case None:
            return []
        case 2:  # Only 1 argument
            if $ARG1 == "all":
                applications = list(filter(lambda x: x.is_dir() and x.stem in DOT_DEPLOYABLES, pg`*/`))
                return list(map(lambda x: x.stem, applications))
            else:
                return [$ARG1]
        case _:
            return applications[1:]

applications = obtain_list_of_apps($ARGS)

if not applications:
    print("No applications provided. Aborting script.")
    exit()

print("Starting list of applications: ", applications)

with open(".fetch-infisical", "r") as file:
    DOT_FETCH_INFISICAL = [line.strip() for line in file]

for app in applications:
    if app in DOT_FETCH_INFISICAL:
        # Get project
        print(f"Fetching secrets for {app}...")
        dotenv_local = dotenv_values(app + "/.env")
        INFISICAL_PROJECT = dotenv_local.get("INFISICAL_PROJECT")

        # Inject secrets and start composition
        infisical run --projectId=@(INFISICAL_PROJECT) --token=$INFISICAL_TOKEN --env=prod -- task up APP=@(app)
    else:
        # Just start composition
        task up APP=@(app)
